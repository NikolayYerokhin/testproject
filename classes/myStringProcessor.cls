public class myStringProcessor implements StringProcessor, StringProcessor2 {
    public static String addPrefix (String str){
        String pre = 'pre_';
        return pre+str;
    }    
    public static String addPostfix (String str){
        String post = '_post';
        return str+post;
    }    
    public static String removeWhitespaces (String str){
        return str.deleteWhitespace();
    }    
    public void updateList (List<String> myList1, List<String> myList2){
        for (String myStr : myList1) {
            String myStr1 = myStringProcessor.addPostfix (myStr);
            myList2.add(myStr1);
        }
    }
    public static Boolean get (String str){
        return str.isAllUpperCase();
    }
}