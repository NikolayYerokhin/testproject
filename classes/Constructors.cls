public class Constructors{
    private Integer side1 = 3;
    private Integer side2 = 2;
    public String newValues(Integer value1, Integer value2) {
        if (this.side1 != null && this.side2 != null) {
            this.side1 = value1;
            this.side2 = value2;
            return null;
        } else {
            return 'Enter data';
        }
    }
    public Constructors(){
        this.side1 = null;
    }
    public Constructors(Integer width){
        this.side1 = width;
    }
    public Constructors(Integer width, Integer height){
        this.side1 = width;
        this.side2 = height;
    }
}