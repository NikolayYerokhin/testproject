public class Rectangle_Class_light {
    private Integer side1 = 3;
    private Integer side2 = 2;
    public String newValues(Integer value1, Integer value2) {
        if (this.side1 != null && this.side2 != null) {
            this.side1 = value1;
            this.side2 = value2;
            return null;
        } else {
            return 'Enter data';
        }
    }
    public String perimeter(Integer side1, Integer side2) {
        if (this.side1 != null && this.side2 != null) {
            Integer perimeter = (this.side1 + this.side2) * 2;
            return String.valueOf(perimeter);
        } else {
            return 'Enter data';
        }
    }
    public String area(Integer side1, Integer side2) {
        if (this.side1 != null && this.side2 != null) {
            Integer area = this.side1 * this.side2;
            return String.valueOf(area);
        } else {
            return 'Enter data';
        }
    }
    public String diagonal(Integer side1, Integer side2) {
        if (this.side1 != null && this.side2 != null) {
            Double diagonal = math.sqrt(math.pow(this.side1, 2) + math.pow(this.side2, 2));
            return String.valueOf(diagonal);
        } else {
            return 'Enter data';
        }

        /*
        Добавьте к основному вашему классу приватный  метод, который в зависимости от входного параметра будет округлять периметр и площадь до заданного знака.
Добавьте конструкторы к классу которые могут принимать 0, 1(ширина), 2(ширина и длина) параметра.
         */

    }
    private String roundMethod(Integer value) {
        Rectangle_Class_light rectangle = new Rectangle_Class_light();
        String perimeter = (rectangle.perimeter(3, 2));
        String area = (rectangle.area(4, 6));
        Decimal p = Decimal.valueOf(perimeter);
        Decimal a = Decimal.valueOf(area);
        Decimal roundPerimeter = p.setScale(value);
        Decimal roundArea = a.setScale(value);
        return null;
    }

    private String ConstructorTest(Integer value1, Integer value2) {
        if (this.side1 != null && this.side2 != null) {
            this.side1 = value1;
            this.side2 = value2;
            return null;
        } else {
            return 'Enter data';
        }
    }

    public Rectangle_Class_light(){}
    public Rectangle_Class_light(Integer width){
        side1 = width;
    }
    public Rectangle_Class_light(Integer width, Integer height){
        side1 = width;
        side2 = height;
    }
}