public class BasicClassesComponent {
    public String FirstName {get; set;}
    public String LastName {get; set;}
    public String UserID {get; set;}
    public String orgCurr {get; set;}
    public String orgEmail {get; set;}
    public String orgName {get; set;}
    public Date today {get; set;}
    public Integer LQ {get; set;}

    public BasicClassesComponent(){
        FirstName = UserInfo.getFirstName();
        LastName = UserInfo.getLastName();
        UserID = UserInfo.getUserId();
        orgCurr = UserInfo.getDefaultCurrency();
        orgEmail = UserInfo.getUserEmail();
        orgName = UserInfo.getOrganizationName();
        today = System.today();
        LQ = Limits.getLimitQueries()-Limits.getQueries();
    }
}