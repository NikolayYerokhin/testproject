public class LoopsTasks {

    /**
   * Returns the 'Fizz','Buzz' or an original number as String using the following rules:
   * 1) return original number as String
   * 2) but if number multiples of three return 'Fizz'
   * 3) for the multiples of five return 'Buzz'
   * 4) for numbers which are multiples of both three and five return 'FizzBuzz'
   *
   * @param {number} num
   * @return {any}
   *
   * @example
   *   2 =>  '2'
   *   3 => 'Fizz'
   *   5 => 'Buzz'
   *   4 => '4'
   *  15 => 'FizzBuzz'
   *  20 => 'Buzz'
   *  21 => 'Fizz'
   *
   */
    public static String getFizzBuzz(Integer num) {
        if (math.mod(num, 15) == 0) {
            return 'FizzBuzz';
        } else if (math.mod(num, 5) == 0) {
            return 'Buzz';
        } else if (math.mod(num, 3) == 0) {
            return 'Fizz';
        } else {
            return String.valueOf(num);
        }
    }

    /**
   * Returns the factorial of the specified integer n.
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   1  => 1
   *   5  => 120
   *   10 => 3628800
   */
    public static Integer getFactorial(Integer num) {
        Integer i;
        Integer res = 1;
        for (i = 2; i <= num; i++)
            res *= i;
        return res;
    }

    /**
   * Returns the sum of integer numbers between n1 and n2 (inclusive).
   *
   * @param {number} n1
   * @param {number} n2
   * @return {number}
   *
   * @example:
   *   1,2   =>  3  ( = 1+2 )
   *   5,10  =>  45 ( = 5+6+7+8+9+10 )
   *   -1,1  =>  0  ( = -1 + 0 + 1 )
   */
    public static Integer getSumBetweenNumbers(Integer num1, Integer num2) {
        Integer n = 0;
        Integer d = num2 - num1;
        for (integer x = num1; x < num2; x++){
            n = n + x;
        }
        return n;
    }

    /**
   * Returns true, if a triangle can be built with the specified sides a,b,c and false in any other ways.
   *
   * @param {number} a
   * @param {number} b
   * @param {number} c
   * @return {bool}
   *
   * @example:
   *   1,2,3    =>  false
   *   3,4,5    =>  true
   *   10,1,1   =>  false
   *   10,10,10 =>  true
   */
    public static Boolean isTriangle(Integer a, Integer b, Integer c) {
        if (a + b <= c) {
            return false;
        } else if (b + c <= a) {
            return false;
        } else if (a + c <= b) {
            return false;
        }else {
            return true;
        }
    }

    /**
   * Returns true, if two specified axis-aligned rectangles overlap, otherwise false.
   * Each rectangle representing by Map<String, Integer>
   *  {
   *     top: 5,
   *     left: 5,
   *     width: 20,
   *     height: 10
   *  }
   *
   *  (5;5)
   *     -------------
   *     |           |
   *     |           |  height = 10
   *     -------------
   *        width=20
   *
   * NOTE: Please use canvas coordinate space (https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes#The_grid),
   * it differs from Cartesian coordinate system.
   *
   * @param {Map<String, Integer>} rect1
   * @param {Map<String, Integer>} rect2
   * @return {bool}
   *
   * @example:
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top: 5, left: 5, width: 20, height: 20 }    =>  true
   *
   *   { top: 0, left: 0, width: 10, height: 10 },
   *   { top:20, left:20, width: 20, height: 20 }    =>  false
   *
   */
    public static Boolean doRectanglesOverlap(Map<String, Integer> rect1, Map<String, Integer> rect2) {
        Integer x1 = rect1.get('top');
        Integer y1 = rect1.get('left')+rect1.get('height');
        Integer x2 = rect1.get('top')+rect1.get('width');
        Integer y2 = rect1.get('left');
        Integer x3 = rect2.get('top');
        Integer y3 = rect2.get('left')+rect2.get('height');
        Integer x4 = rect2.get('top')+rect1.get('width');
        Integer y4 = rect2.get('left');
        if (x3 > x2 || y3 < y2 || x1 > x4 || y1 < y4) {
            return false;
        } else {
            return true;
        }
    }

    /**
   * Returns true, if point lies inside the circle, otherwise false.
   * Circle is:
   *     Center Map<String, Double>: {
   *       x: 5,
   *       y: 5
   *     },
   *     Radius Integer: 20
   * Point is Map<String, Double>
   *  {
   *     x: 5,
   *     y: 5
   *  }
   *
   * @param {Map<String, Double>} center
   * @param {Integer} raduis
   * @param {Map<String, Double>} point
   * @return {bool}
   *
   * @example:
   *   center: { x:0, y:0 }, radius: 10, point: { x:0, y:0 }     => true
   *   center: { x:0, y:0 }, radius:10,  point: { x:10, y:10 }   => false
   *
   */
    public static Boolean isInsideCircle(Map<String, Double> center, Integer radius, Map<String, Double> point) {

        if (((math.pow(point.get('x')-center.get('x'),2))+(math.pow(point.get('y')-center.get('y'),2)))<(math.pow(radius,2))) {
        return true;
    } else {
            return false;
        }
    }

    /**
   * Returns the first non repeated char in the specified strings otherwise returns null.
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   *   'The quick brown fox jumps over the lazy dog' => 'T'
   *   'abracadabra'  => 'c'
   *   'entente' => null
   */
    public static String findFirstSingleChar(String str) {
        String [] s = str.split ('');
        String k;
        for (Integer i=0;i<str.length();i++){
            Integer j = str.countMatches(s[i]);
            if (j==1){k=str.substring(i,i+1);
                break;
            }
        }
        return k;
    }

    /**
   * Returns the string representation of math interval, specified by two points and include / exclude flags.
   * See the details: https://en.wikipedia.org/wiki/Interval_(mathematics)
   *
   * Please take attention, that the smaller number should be the first in the notation
   *
   * @param {number} a
   * @param {number} b
   * @param {bool} isStartIncluded
   * @param {bool} isEndIncluded
   * @return {string}
   *
   * @example
   *   0, 1, true, true   => '[0, 1]'
   *   0, 1, true, false  => '[0, 1)'
   *   0, 1, false, true  => '(0, 1]'
   *   0, 1, false, false => '(0, 1)'
   * Smaller number has to be first :
   *   5, 3, true, true   => '[3, 5]'
   *
   */
    public static String getIntervalString(Integer a, Integer b, Boolean isStartIncluded, Boolean isEndIncluded) {
        String str;
        if (b>a){
            str = String.valueOf(a) + ', ' + String.valueOf(b);
        } else {
            str = String.valueOf(b) + ', ' + String.valueOf(a);
        }

        if (isStartIncluded == true && isEndIncluded == true){
            return ('['+str+']');
        } else if (isStartIncluded == true && isEndIncluded == false) {
            return ('['+str+')');
        }
        else if (isStartIncluded == false && isEndIncluded == true) {
            return ('('+str+']');
        }
        else {
            return ('('+str+')');
        }
    }

    /**
   * Reverse the specified string (put all chars in reverse order)
   *
   * @param {string} str
   * @return {string}
   *
   * @example:
   * 'The quick brown fox jumps over the lazy dog' => 'god yzal eht revo spmuj xof nworb kciuq ehT'
   * 'abracadabra' => 'arbadacarba'
   * 'rotator' => 'rotator'
   * 'noon' => 'noon'
   */
    public static String reverseString(String str) {
        return str.reverse();
    }

    /**
   * Reverse the specified integer number (put all digits in reverse order)
   *
   * @param {number} num
   * @return {number}
   *
   * @example:
   *   12345 => 54321
   *   1111  => 1111
   *   87354 => 45378
   *   34143 => 34143
   */
    public static Integer reverseInteger(Integer num) {
        String a = String.valueOf(num).reverse();
        Integer b = Integer.valueOf(a);
        return b;
    }

    /**
   * Returns the digital root of integer:
   *   step1 : find sum of all digits
   *   step2 : if sum > 9 then goto step1 otherwise return the sum
   *
   * @param {number} n
   * @return {number}
   *
   * @example:
   *   12345 ( 1+2+3+4+5 = 15, 1+5 = 6) => 6
   *   23456 ( 2+3+4+5+6 = 20, 2+0 = 2) => 2
   *   10000 ( 1+0+0+0+0 = 1 ) => 1
   *   165536 (1+6+5+5+3+6 = 26,  2+6 = 8) => 8
   */
    public static Integer getDigitalRoot(Integer num) {
        Integer sum = 0;
        while (true) {
            if (num > 0) {
                Integer dsum = math.mod(num, 10);
                num /= 10;
                sum += dsum;
            } else if (sum > 9) {
                Integer dsum = math.mod(sum, 10);
                sum /= 10;
                sum += dsum;
            } else if (sum <= 9) break;
        }
        return sum;
    }

    /**
   * Returns true if the specified string has the balanced brackets and false otherwise.
   * Balanced means that is, whether it consists entirely of pairs of opening/closing brackets
   * (in that order), none of which mis-nest.
   * Brackets include [],(),{},<>
   *
   * @param {string} str
   * @return {boolean}
   *
   * @example:
   *   '' => true
   *   '[]'  => true
   *   '{}'  => true
   *   '()   => true
   *   '[[]' => false
   *   ']['  => false
   *   '[[][][[]]]' => true
   *   '[[][]][' => false
   *   '{)' = false
   *   '{[(<{[]}>)]}' = true
   */
    public static Boolean isBracketsBalanced(String str) {
        String[] Mas = str.split('');
        List<String> openBracket = new List<String>{
                '[', '{', '(', '<'
        };
        List<String> closeBracket = new List<String>{
                ']', '}', ')', '>'
        };
        if (str == '')
            return true;
        if (!openBracket.contains(Mas[0]) || (str.length() - 2 * (str.length() / 2)) == 1 || !closeBracket.contains(Mas[str.length() - 1])) {
            return false;
        }
        for (integer i = 0; i < Mas.size() - 1; i++) {
            if (openBracket.contains(Mas[i]) && closeBracket.contains(Mas[i + 1])) {
                if (openBracket.indexOf(Mas[i]) != closeBracket.indexOf(Mas[i + 1])) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
   * Returns the human readable string of time period specified by the start and end time.
   * The result string should be constrcuted using the folliwing rules:
   *
   * ---------------------------------------------------------------------
   *   Difference                 |  Result
   * ---------------------------------------------------------------------
   *    0 to 45 seconds           |  a few seconds ago
   *   45 to 90 seconds           |  a minute ago
   *   90 seconds to 45 minutes   |  2 minutes ago ... 45 minutes ago
   *   45 to 90 minutes           |  an hour ago
   *  90 minutes to 22 hours      |  2 hours ago ... 22 hours ago
   *  22 to 36 hours              |  a day ago
   *  36 hours to 25 days         |  2 days ago ... 25 days ago
   *  25 to 45 days               |  a month ago
   *  45 to 345 days              |  2 months ago ... 11 months ago
   *  345 to 545 days (1.5 years) |  a year ago
   *  546 days+                   |  2 years ago ... 20 years ago
   * ---------------------------------------------------------------------
   *
   * @param {DateTime} startDate
   * @param {DateTime} endDate
   * @return {string}
   *
   * @example
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:01')  => 'a few seconds ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-01 01:00:05')  => '5 minutes ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2000-01-02 03:00:05')  => 'a day ago'
   *   DateTime('2000-01-01 01:00:00'), DateTime('2015-01-02 03:00:05')  => '15 years ago'
   *
   */
    public static String timespanToHumanString(DateTime startDate, DateTime endDate) {
        Integer elapsedYears = endDate.year() - startDate.year();
        Integer elapsedMonths = endDate.month() - startDate.month();
        Integer elapsedDays = endDate.day() - startDate.day();
        Integer elapsedHours = endDate.hour() - startDate.hour();
        Integer elapsedMinutes = endDate.minute() - startDate.minute();
        Integer elapsedSeconds = endDate.second() - startDate.second();
        if (elapsedSeconds <= 45 && elapsedMinutes < 1 && elapsedHours == 0 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0) {
            return ('a few seconds ago');
        } else if (elapsedSeconds <= 30 && elapsedMinutes == 1 && elapsedHours == 0 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0) {
            return ('a minute ago');
        } else if (elapsedSeconds > 45 && elapsedMinutes == 0 && elapsedHours == 0 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0) {
            return ('a minute ago');
        } else if (elapsedSeconds > 30 && elapsedMinutes == 1 && elapsedHours == 0 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0) {
            return ('2 minutes ago');
        } else if (elapsedMinutes>=2 && elapsedMinutes<45 && elapsedHours<1 && elapsedDays==0 && elapsedYears==0 && elapsedMonths==0 && elapsedSeconds>=0) {
            return (elapsedMinutes + ' minutes ago');
        } else if (elapsedMinutes==45 && elapsedHours<1 && elapsedDays==0 && elapsedYears==0 && elapsedMonths==0 && elapsedSeconds==0){
            return (elapsedMinutes + ' minutes ago');
        } else if (elapsedMinutes==45 && elapsedHours<1 && elapsedDays==0 && elapsedYears==0 && elapsedMonths==0 && elapsedSeconds>0){
            return ('an hour ago');
        } else if (elapsedMinutes >= 45 && elapsedHours < 1 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds > 0) {
            return ('an hour ago');
        } else if (elapsedHours == 1 && elapsedMinutes <= 30 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds == 0) {
            return ('an hour ago');
        } else if (elapsedHours == 1 && elapsedMinutes >= 30 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds > 0) {
            return ('2 hours ago');
        } else if (elapsedHours > 1 && elapsedHours <= 22 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0 && elapsedMinutes <= 30 && elapsedSeconds == 0) {
            return (elapsedHours + ' hours ago');
        } else if (elapsedHours > 1 && elapsedHours <= 22 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0 && elapsedMinutes >= 30) {
            return ((elapsedHours + 1) + ' hours ago');
        } else if (elapsedHours >= 22 && elapsedDays == 0 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds > 0) {
            return ('a day ago');
        } else if (elapsedDays == 1 && elapsedHours <= 12 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds == 0) {
            return ('a day ago');
        } else if (elapsedDays == 1 && elapsedHours >= 11 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds > 0) {
            return ('2 days ago');
        } else if (elapsedDays > 1 && elapsedDays <= 25 && elapsedYears == 0 && elapsedMonths == 0 && elapsedSeconds == 0) {
            return (elapsedDays + ' days ago');
        } else if (elapsedDays >= 25 && elapsedMonths < 1 && elapsedYears == 0 && elapsedSeconds > 0) {
            return ('a month ago');
        } else if (elapsedDays <= 14 && elapsedMonths == 1 && elapsedYears == 0) {
            return ('a month ago');
        } else if (elapsedDays > 14 && elapsedMonths == 1 && elapsedYears == 0) {
            return ('2 months ago');
        } else if (elapsedMonths <= 11 && elapsedYears == 0 && elapsedDays <= 10) {
            return (elapsedMonths + ' months ago');
        } else if (elapsedMonths < 11 && elapsedYears == 0 && elapsedDays >= 11) {
            return (elapsedMonths + 1 + ' months ago');
        } else if (elapsedMonths >= 11 && elapsedYears == 0 && elapsedDays >= 11) {
            return ('a year ago');
        } else if (elapsedMonths <= 6 && elapsedYears == 1) {
            return ('a year ago');
        } else if (elapsedMonths > 6 && elapsedYears == 1) {
            return ('2 years ago');
        } else {
            return (elapsedYears + ' years ago');
        }
    }
}